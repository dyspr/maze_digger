var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  walker: [0, 0, 0],
  path: [128, 128, 128],
  back: [64, 64, 64]
}

var size = 11
var boardSize
var mazeMap = create2DArray(size, size, 1, true)
var walker = {
  x: null,
  y: null
}
var neighbours = []
walker.x = Math.floor(Math.random() * (size - 1) * 0.5) * 2 + 1
walker.y = Math.floor(Math.random() * (size - 1) * 0.5) * 2 + 1
mazeMap[walker.x][walker.y] = 0
var backTrack = []
var frames = 0
var inProg = true

var checkMap = create2DArray(size, size, 0, true)

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < mazeMap.length; i++) {
    for (var j = 0; j < mazeMap[i].length; j++) {
      noStroke()
      if (mazeMap[i][j] === 1) {
        fill(colors.dark)
      } else {
        fill(128 + 128 * mazeMap[i][j] * 2)
      }
      rect(windowWidth * 0.5 + (i - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (43 / 768) * boardSize * (17 / size), (43 / 768) * boardSize * (17 / size))
    }
  }

  for (i = 0; i < backTrack.length; i++) {
    noStroke()
    fill(128 + 128 * mazeMap[backTrack[i][0]][backTrack[i][1]] * 2)
    rect(windowWidth * 0.5 + (backTrack[i][0] - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (backTrack[i][1] - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (43 / 768) * boardSize * (17 / size), (43 / 768) * boardSize * (17 / size))
  }

  fill(colors.walker)
  noStroke()
  ellipse(windowWidth * 0.5 + (walker.x - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (walker.y - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (24 / 768) * boardSize * (17 / size), (24 / 768) * boardSize * (17 / size))

  frames += deltaTime * 0.025
  if (frames > 1 && inProg === true) {
    frames = 0

    for (var i = 0; i < mazeMap.length; i++) {
      for (var j = 0; j < mazeMap[i].length; j++) {
        if (mazeMap[i][j] !== 1) {
          checkMap[i][j] = mazeMap[i][j]
        }
      }
    }

    for (var i = 0; i < mazeMap.length; i++) {
      for (var j = 0; j < mazeMap[i].length; j++) {
        if (mazeMap[i][j] !== 1 && mazeMap[i][j] > 0) {
          mazeMap[i][j] -=  0.001 * (57 / size)
        } else if (mazeMap[i][j] !== 1 && mazeMap[i][j] <= 0) {
          mazeMap[i][j] =  0
        }
      }
    }

    neighbours = getNeighbours(walker.x, walker.y, mazeMap)
    var rand = Math.floor(Math.random() * neighbours.length)
    if (neighbours[rand] !== undefined) {
      backTrack.push([walker.x, walker.y])
      walker.x = neighbours[rand][0]
      walker.y = neighbours[rand][1]
      mazeMap[walker.x][walker.y] = 0.5
      if (neighbours[rand][2] === 0) {
        mazeMap[walker.x - 1][walker.y] = 0.5
        backTrack.push([walker.x - 1, walker.y])
      } else if (neighbours[rand][2] === 1) {
        mazeMap[walker.x + 1][walker.y] = 0.5
        backTrack.push([walker.x + 1, walker.y])
      } else if (neighbours[rand][2] === 2) {
        mazeMap[walker.x][walker.y - 1] = 0.5
        backTrack.push([walker.x, walker.y - 1])
      } else if (neighbours[rand][2] === 3) {
        mazeMap[walker.x][walker.y + 1] = 0.5
        backTrack.push([walker.x, walker.y + 1])
      }
    } else {
      if (backTrack.length !== 0) {
        walker.x = backTrack[backTrack.length - 2][0]
        walker.y = backTrack[backTrack.length - 2][1]
        backTrack.splice(-2, 2)
      } else {
        if (Math.max(...checkMap.flat()) === 0) {
          size = Math.floor(mouseX / windowWidth * 24) * 2 + 11
          mazeMap = create2DArray(size, size, 1, true)
          walker.x = Math.floor(Math.random() * (size - 1) * 0.5) * 2 + 1
          walker.y = Math.floor(Math.random() * (size - 1) * 0.5) * 2 + 1
          backTrack = []
          neighbours = []
          inProg = false
          setTimeout(function() {
            inProg = true
          })
          checkMap = create2DArray(size, size, 0, true)
        }
      }
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}

function getNeighbours(x, y, array) {
  var neighbour = []
  if (x < array.length - 2) {
    if (array[x + 2][y] === 1) {
      neighbour.push([x + 2, y, 0])
    }
  }
  if (x > 2) {
    if (array[x - 2][y] === 1) {
      neighbour.push([x - 2, y, 1])
    }
  }
  if (y < array.length - 2) {
    if (array[x][y + 2] === 1) {
      neighbour.push([x, y + 2, 2])
    }
  }
  if (y > 2) {
    if (array[x][y - 2] === 1) {
      neighbour.push([x, y - 2, 3])
    }
  }
  return neighbour
}
